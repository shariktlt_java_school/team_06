package ru.edu.project.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.da.AdminDALayer;
import ru.edu.project.backend.interfaces.AdminService;
import ru.edu.project.backend.model.StudentInfo;
import ru.edu.project.backend.model.StuffInfo;

@Service
@Profile("!STUB")
@Qualifier("StuffServiceLayer")
public class AdminServiceLayer implements AdminService {

    /**
     * Зависимость для слоя доступа к данным услуг.
     */
    @Autowired
    private AdminDALayer daLayer;


    /**
     * Добавить студента.
     *
     * @param student
     * @return StuffInfo
     */
    @Override
    public int createStudent(final StudentInfo student) {
        return daLayer.addNewStudent(student);
    }

    /**
     * Удалить студента.
     *
     * @param id
     * @return StudentInfo
     */
    @Override
    public int deleteStudent(Long id) {
        return daLayer.deleteStudent(id);
    }

    /**
     * Добавить сотрудника.
     *
     * @param stuff
     * @return StuffInfo
     */
    @Override
    public int createStuff(StuffInfo stuff) {
        return daLayer.addNewStuff(stuff);
    }

    /**
     * Удалить сотрудника.
     *
     * @param id
     * @return StuffInfo
     */
    @Override
    public int deleteStuff(Long id) {
        return daLayer.deleteStuff(id);
    }
}
