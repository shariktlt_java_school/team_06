package ru.edu.project.backend.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Setter
@Jacksonized
@Builder
public class GroupInfo {

    /**
     * Идентификатор группы.
     */
    private String id;

    /**
     * Название группы.
     */
    private String title;

    /**
     * Флаг начала занятий.
     */
    private boolean isStarted;

}
