package ru.edu.project.backend.interfaces;

import ru.edu.project.backend.common.AcceptorArgument;
import ru.edu.project.backend.model.Group;
import ru.edu.project.backend.model.GroupForm;

import java.util.List;

public interface GroupService {

    /**
     * Создать группу.
     * @param form Data from Group form
     * @return new Group object
     */
    @AcceptorArgument
    Group createGroup(GroupForm form);

    /**
     * Получить все группы из БД.
     * @return list of Group objects
     */
    List<Group> getGroups();

    /**
     * Обновить информацию о группе.
     * @param id Group id
     * @param form Data from Group form
     * @return updated Group object
     */
    @AcceptorArgument
    Group updateGroup(String id, GroupForm form);

    /**
     * Получить информацию о группе.
     * @param id Group id
     * @return Group object
     */
    Group getGroup(String id);


    /**
     * Удалить группу по идентификатору
     * @param id Group id
     * @return Group was deleted?
     */
    @AcceptorArgument
    boolean deleteGroup(String id);

}
