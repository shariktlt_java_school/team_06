package ru.edu.project.backend.da;

import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import ru.edu.project.backend.model.Grades;
import ru.edu.project.backend.model.StudentInfo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Data Access слой.
 */
@Component
@Profile("JDBC_TEMPLATE")
public class StudentDA implements StudentDALayer {
    /**
     * javadoc.
     */
    private static Logger logger = LoggerFactory.getLogger(StudentDA.class);

    /**
     * Запрос поиска записей по списку id.
     */
    public static final String SELECT_STUDENT_BY_ID = "SELECT * FROM STUDENTS WHERE student_id=?";

    /**
     * Запрос поиска успеваемости по id.
     */
    public static final String SELECT_GRADES_BY_ID = "SELECT * FROM GRADES WHERE student_id=?";

    /**
     * Запрос пароля по id.
     */
    public static final String SELECT_PASSWORD_BY_ID = "SELECT password FROM STUDENTS WHERE student_id=?";


    /**
     * Зависимость на шаблон jdbc.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Зависимость на шаблон jdbc named.
     */
    @Autowired
    private NamedParameterJdbcTemplate jdbcNamed;

    /**
     * Получаем услугу по id.
     *
     * @param id
     * @return job
     */
    @Override
    public StudentInfo getStudentInfoById(final Long id) {
        logger.info(id.toString());
        return jdbcTemplate.query(SELECT_STUDENT_BY_ID, this::singleRowMapper, id);
    }

    /**
     * Получение student progress по id.
     *
     * @param id
     * @return
     */
    @Override
    public List<Grades> getProgressById(final Long id) {
        List<Grades> nullRequestList = new ArrayList<>();
        Grades grades = Grades.builder()
                .student_id(id)
                .grade(0D)
                .group_id(0L)
                .subject_title("предметов не найдено")
                .build();
        nullRequestList.add(grades);
        List<Grades> list = new ArrayList<>();
        try {
            list = jdbcTemplate.query(SELECT_GRADES_BY_ID, this::rowMapper, id);
        } catch (Exception e) {
            //пропустить
        }
        return list.size() == 0? nullRequestList : list;
    }

    /**
     * получить пароль  по id.
     *
     * @param id
     * @return
     */
    @Override
    public Map<String, String> getPasswordById(Long id) {
        String password = jdbcTemplate.query(SELECT_PASSWORD_BY_ID, this::StringMapper, id).get(0);
        Map<String, String> response = new HashMap<>();
        response.put("role", "student");
        response.put("password", password);
        return response;
    }

    @SneakyThrows
    private String StringMapper(ResultSet resultSet, int i) {
        return resultSet.getString("password");
    }

    @SneakyThrows
    private Grades rowMapper(final ResultSet resultSet, final int pos) {
        Grades row = Grades.builder()
                .student_id(resultSet.getLong("student_id"))
                .group_id(resultSet.getLong("group_id"))
                .subject_title(resultSet.getString("subject_title"))
                .grade(resultSet.getDouble("grade"))
                .build();
        return row;
    }

    @SneakyThrows
    private StudentInfo singleRowMapper(final ResultSet resultSet) {
        resultSet.next();
        return mapRow(resultSet);
    }

    private StudentInfo mapRow(final ResultSet resultSet) throws SQLException {
        return StudentInfo.builder()
                .id(resultSet.getLong("student_id"))
                .firstName(resultSet.getString("first_name"))
                .secondName(resultSet.getString("second_name"))
                .lastName(resultSet.getString("last_name"))
                .groupId(resultSet.getLong("group_id"))
                .build();
    }

}
