package ru.edu.project.backend.da;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;
import ru.edu.project.backend.model.StudentInfo;
import ru.edu.project.backend.model.StuffInfo;

import java.util.HashMap;
import java.util.Map;

/**
 * Data Access слой.
 */
@Component
@Profile("JDBC_TEMPLATE")
public class AdminDA implements AdminDALayer {
    /**
     * javadoc.
     */
    private static Logger logger = LoggerFactory.getLogger(AdminDA.class);

    /**
     * Запрос удаление студента.
     */
    public static final String DELETE_STUDENT = "DELETE FROM Students WHERE student_id=?";

    /**
     * Запрос удаление сотрудника.
     */
    public static final String DELETE_STUFF = "DELETE FROM Stuff WHERE stuff_id=?";

    /**
     * Зависимость на шаблон jdbc.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Зависимость на шаблон jdbc named.
     */
    @Autowired
    private NamedParameterJdbcTemplate jdbcNamed;

    /**
     * Зависимость на шаблон jdbc insert.
     */
    private SimpleJdbcInsert jdbcInsertStudent;


    /**
     * Внедрение зависимости jdbc insert с настройкой для таблицы.
     *
     * @param bean
     */
    @Autowired
    public void setJdbcInsertStudent(final SimpleJdbcInsert bean) {
        jdbcInsertStudent = bean
                .withTableName("students")
                .usingGeneratedKeyColumns("student_id");
    }
    /**
     * Зависимость на шаблон jdbc insert.
     */
    private SimpleJdbcInsert jdbcInsertStuff;

    /**
     * Внедрение зависимости jdbc insert с настройкой для таблицы.
     *
     * @param bean
     */
    @Autowired
    public void setJdbcInsertStuff(final SimpleJdbcInsert bean) {
        jdbcInsertStuff = bean
                .withTableName("stuff")
                .usingGeneratedKeyColumns("stuff_id");
    }

    /**
     * Добавить студента.
     *
     * @return StudentInfo
     */
    @Override
    public int addNewStudent(final StudentInfo student) {
        long id = jdbcInsertStudent.executeAndReturnKey(studentToMap(student)).longValue();
        student.setId(id);
        return (int) id;
    }

    private Map<String, Object> studentToMap(final StudentInfo student) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("first_name", student.getFirstName());
        map.put("second_name", student.getSecondName());
        map.put("last_name", student.getLastName());
        map.put("password", student.getPassword());
        return map;
    }

    /**
     * Удалить студента.
     *
     * @param id
     * @return StudentInfo
     */
    @Override
    public int deleteStudent(Long id) {
        int i = jdbcTemplate.update(DELETE_STUDENT, id);
        return i;
    }

    /**
     * Добавить сотрудника.
     *
     * @return StuffInfo
     */
    @Override
    public int addNewStuff(final StuffInfo stuff) {
        long id = jdbcInsertStuff.executeAndReturnKey(stuffToMap(stuff)).longValue();
        stuff.setId(id);
        return (int) id;
    }

    private Map<String, Object> stuffToMap(final StuffInfo stuff) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("first_name", stuff.getFirstName());
        map.put("second_name", stuff.getSecondName());
        map.put("last_name", stuff.getLastName());
        map.put("position", stuff.getPosition());
        map.put("password", stuff.getPassword());
        return map;
    }

    /**
     * Удалить сотрудника.
     *
     * @param id
     * @return StuffInfo
     */
    @Override
    public int deleteStuff(Long id) {
        int i = jdbcTemplate.update(DELETE_STUFF, id);
        return i;
    }
}
