package ru.edu.project.backend.da;

import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.model.Group;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Profile("JDBC_TEMPLATE")
public class GroupDA implements GroupDALayer {

    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(GroupDA.class);

    /**
     * Выбрать все группы.
     */
    public static final String SQL_SELECT_ALL =
            "SELECT * FROM groups";

    /**
     * Удалить все группы.
     */
    public static final String SQL_DELETE_ALL =
            "DELETE FROM groups";

    /**
     * Выбрать группу по id.
     */
    public static final String SQL_WHERE_ID =
            " WHERE group_id = ?";

    /**
     * Обновить данные группы.
     */
    public static final String SQL_UPDATE_BY_ID =
            "UPDATE groups SET "
                    + "group_title = :group_title, "
                    + "group_is_started = :group_is_started"
                    + " WHERE group_id = :group_id";

    /**
     * Зависимость на шаблон jdbc.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Зависимость на шаблон jdbc insert.
     */
    @Autowired
    private SimpleJdbcInsert jdbcInsert;

    /**
     * Зависимость на шаблон jdbc named.
     */
    @Autowired
    private NamedParameterJdbcTemplate jdbcNamed;

    /**
     * Внедрение зависимости jdbc insert с настройкой для таблицы.
     *
     * @param bean SimpleJdbcInsert Bean
     */
    @Autowired
    public void setJdbcInsert(final SimpleJdbcInsert bean) {
        jdbcInsert = bean
                .withTableName("groups")
                .usingGeneratedKeyColumns("group_id");
    }

    /**
     * Сохранить информацию о группе.
     * @param group Group object
     * @return Group object
     */
    @Override
    public Group save(final Group group) {
        if (group.getId() == null) {
            return insert(group);
        }
        return update(group);
    }

    private Group update(final Group group) {
        jdbcNamed.update(SQL_UPDATE_BY_ID, toMap(group));
        return group;
    }

    private Group insert(final Group group) {
        Long id = jdbcInsert.executeAndReturnKey(toMap(group)).longValue();
        group.setId(String.valueOf(id));
        return group;
    }

    /**
     * Получить группу по идентификатору.
     * @param id Group id
     * @return Group object
     */
    @Override
    public Group getById(final String id) {
        return jdbcTemplate.query(SQL_SELECT_ALL + SQL_WHERE_ID,
                this::singleRowMapper,
                id);
    }

    /**
     * @param id Group id
     * @return true if Group was deleted successfully
     */
    @Override
    public Boolean deleteById(final String id) {
        int result = jdbcTemplate.update(SQL_DELETE_ALL + SQL_WHERE_ID, id);
        return result == 1;
    }

    /**
     * @return List of Group objects
     */
    @Override
    public List<Group> getGroups() {
        return jdbcTemplate.query(SQL_SELECT_ALL,
                this::rowMapper);
    }


    private Map<String, Object> toMap(final Group group) {
        HashMap<String, Object> map = new HashMap<>();

        if (group.getId() != null) {
            map.put("group_id", Long.valueOf(group.getId()));
        }

        map.put("group_title", group.getTitle());
        map.put("group_is_started", group.isStarted());

        return map;
    }

    @SneakyThrows
    private Group rowMapper(final ResultSet rs,
                            final int pos) {
        return mapRow(rs);
    }

    @SneakyThrows
    private Group singleRowMapper(final ResultSet rs) {
        if (rs.next()) {
            return mapRow(rs);
        } else {
            return null;
        }
    }

    private Group mapRow(final ResultSet rs)
            throws SQLException {
        return Group.builder()
                .id(String.valueOf(rs.getLong("group_id")))
                .title(rs.getString("group_title"))
                .isStarted(rs.getBoolean("group_is_started"))
                .build();
    }

}
