package ru.edu.project.backend.da;

import ru.edu.project.backend.model.Group;

import java.util.List;

public interface GroupDALayer {

    /**
     * Сохранить информацию о группе.
     * @param group Group object
     * @return Group object
     */
    Group save(Group group);

    /**
     * Получить группу по идентификатору.
     * @param id Group id
     * @return Group object
     */
    Group getById(String id);

    /**
     * Удалить группу по идентификатору.
     * @param id Group id
     * @return Group was deleted?
     */
    Boolean deleteById(String id);

    /**
     * ПОлучить все группы из БД.
     * @return List of Group objects
     */
    List<Group> getGroups();



}
