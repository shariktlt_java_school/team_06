package ru.edu.project.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.project.backend.interfaces.StuffService;
import ru.edu.project.backend.model.Grades;
import ru.edu.project.backend.model.StudentInfo;
import ru.edu.project.backend.model.StuffInfo;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/stuff")
public class StuffController implements StuffService {

    /**
     * Делегат для передачи вызова.
     */
    @Autowired
    private StuffService delegate;

    /**
     * Просмотр информации о сотруднике.
     *
     * @param id
     * @return StufftInfo
     */
    @Override
    @GetMapping("/getDetailedInfo/{id}")
    public StuffInfo getDetailedInfo(@PathVariable("id") final Long id) {
        return delegate.getDetailedInfo(id);
    }

    /**
     * Просмотр информации об успеваемости студента.
     *
     * @param id
     * @return StudentProgress
     */
    @Override
    @GetMapping("/getProgress/{id}")
    public List<Grades> getProgress(@PathVariable("id") final Long id) {
        return delegate.getProgress(id);
    }

    /**
     * получение пароля по id.
     *
     * @param id
     * @return
     */
    @Override
    @GetMapping("/getAuthData/{id}")
    public Map<String, String> getAuthData(@PathVariable("id") final Long id) {
        return delegate.getAuthData(id);
    }

    /**
     * Вывод студентов по номеру группы.
     *
     * @param groupId
     * @return List<StudentInfo>
     */
    @Override
    @GetMapping("/getStudentsByGroupId/{group_id}")
    public List<StudentInfo> getStudentsByGroupId(@PathVariable("group_id")Long groupId) {
        return delegate.getStudentsByGroupId(groupId);
    }

    /**
     * Вывод всех студентов.
     *
     * @return List
     */
    @Override
    @GetMapping("/getAllStudents")
    public List<StudentInfo> getAllStudents() {
        return delegate.getAllStudents();
    }
}
