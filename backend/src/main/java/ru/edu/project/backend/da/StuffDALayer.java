package ru.edu.project.backend.da;

import ru.edu.project.backend.model.Grades;
import ru.edu.project.backend.model.StudentInfo;
import ru.edu.project.backend.model.StuffInfo;

import java.util.List;
import java.util.Map;

public interface StuffDALayer {

    /**
     * Получение student info по id.
     *
     * @param id
     * @return job
     */
    StuffInfo getStuffInfoById(Long id);

    /**
     * Получение student progress по id.
     * @param id
     * @return э
     */
    List<Grades> getProgressById(Long id);

    /**
     * получить пароль  по id.
     * @param id
     * @return
     */
    Map<String, String> getPasswordById(Long id);
    /**
     * получить пароль  по id.
     * @return
     */
    List<StudentInfo> getAllStudents();

    /**
     * Вывод студентов по номеру группы.
     *
     * @return List<StudentInfo>
     */
    List<StudentInfo> getStudentsByGroupId(Long groupId);
}
