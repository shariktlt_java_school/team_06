package ru.edu.project.backend.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;

import java.util.Objects;

@Getter
@Setter
@Builder
@Jacksonized
public class StudentInfo {
    /**
     * id студента.
     */
    private Long id;

    /**
     * id группы студента.
     */
    private Long groupId;

    /**
     * Имя студента.
     */
    private String firstName;

    /**
     * Отчество студента.
     */
    private String secondName;

    /**
     * Фамилия студента.
     */
    private String lastName;

    /**
     * Password.
     */
    private String password;

    /**
     * Средний балл.
     */
    private Double average;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StudentInfo that = (StudentInfo) o;
        return Objects.equals(firstName, that.firstName) && Objects.equals(secondName, that.secondName) && Objects.equals(lastName, that.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, secondName, lastName);
    }

    @Override
    public String toString() {
        return "StudentInfo{" +
                "id=" + id +
                ", groupId=" + groupId +
                ", firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
