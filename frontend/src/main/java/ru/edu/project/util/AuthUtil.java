package ru.edu.project.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

public class AuthUtil {

    public static boolean isAuthenticated(HttpServletRequest request, List<String> role) {
        HttpSession session = request.getSession();
        String authObject = (String)session.getAttribute("authObject");
        return role.contains(authObject.toLowerCase());
    }
}
