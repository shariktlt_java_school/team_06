package ru.edu.project.frontend;

import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.project.backend.interfaces.StuffService;
import ru.edu.project.backend.model.StudentInfo;
import ru.edu.project.backend.model.StuffInfo;
import ru.edu.project.util.AuthUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static java.awt.SystemColor.info;

@RequestMapping("/stuff")
@Controller
public class FrontControllerStuff {
    private static final String ADMIN = "admin";
    private static final String MENTOR = "mentor";
    private static final String RECTOR = "rector";
    /**
     * logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(FrontControllerStuff.class);

    /**
     * Ссылка на сервис сотрудника.
     */
    @Autowired
    private StuffService stuffService;

    /**
     * Stuff authentication.
     *
     * @return view
     */
    @SneakyThrows
    @GetMapping()
    public String stuff(HttpServletRequest request,
                        HttpServletResponse response) {
        if (!AuthUtil.isAuthenticated(request, Arrays.asList(ADMIN, MENTOR, RECTOR))) {
            response.sendRedirect("/");
            return null;
        }
        return "stuff";
    }

    /**
     * Get stuff info by id.
     *
     * @return modelAndView
     */
    @SneakyThrows
    @GetMapping("/info")
    public ModelAndView info(HttpServletRequest request,
                             HttpServletResponse response) {

        if (!AuthUtil.isAuthenticated(request, Arrays.asList(ADMIN, MENTOR, RECTOR))) {
            response.sendRedirect("/stuff");
            return null;
        }
        Long id = (Long) request.getSession().getAttribute("id");

        ModelAndView model = new ModelAndView("stuff_info");
        StuffInfo info = stuffService.getDetailedInfo(id);
        if (info == null) {
            model.setStatus(HttpStatus.NOT_FOUND);
            model.setViewName("task/viewNotFound");
            return model;
        }
        model.addObject("record", info);
        return model;
    }

    /**
     * Get stuff info by id.
     *
     * @return modelAndView
     */
    @SneakyThrows
    @GetMapping("/getAllStudents")
    public ModelAndView getAllStudents(HttpServletRequest request,
                             HttpServletResponse response) {

        if (!AuthUtil.isAuthenticated(request, Arrays.asList(ADMIN, RECTOR))) {
            response.sendRedirect("/stuff");
//            response.sendRedirect("/auth/stuffAuth");
            return null;
        }

        ModelAndView model = new ModelAndView("stuff_all");
        List<StudentInfo> students = stuffService.getAllStudents();
        if (info == null) {
            model.setStatus(HttpStatus.NOT_FOUND);
            model.setViewName("task/viewNotFound");
            return model;
        }
        double allStudentsAverage = students.stream().collect(Collectors.averagingDouble(x -> x.getAverage()));
        model.addObject("allStudentsAverage", allStudentsAverage);
        model.addObject("recordsCount", students.size());
        model.addObject("record", students);
        return model;
    }

    @SneakyThrows
    @GetMapping("/getStudentsByGroupId")
    public ModelAndView getStudentsByGroupId(HttpServletRequest request,
                                       HttpServletResponse response,
                                       @RequestParam("group_id") Long groupId){

        if (!AuthUtil.isAuthenticated(request, Arrays.asList(ADMIN, RECTOR, MENTOR))) {
            response.sendRedirect("/stuff");
//            response.sendRedirect("/auth/stuffAuth");
            return null;
        }

        ModelAndView model = new ModelAndView("stuff_group");
        List<StudentInfo> students = stuffService.getStudentsByGroupId(groupId);
        if (info == null) {
            model.setStatus(HttpStatus.NOT_FOUND);
            model.setViewName("task/viewNotFound");
            return model;
        }
        double allStudentsAverage = students.stream().collect(Collectors.averagingDouble(x -> x.getAverage()));
        model.addObject("allStudentsAverage", allStudentsAverage);
        model.addObject("group_id", groupId);
        model.addObject("recordsCount", students.size());
        model.addObject("record", students);
        return model;
    }

}
