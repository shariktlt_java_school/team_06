package ru.edu.project.backend.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Setter
@Builder
@Jacksonized
public class StuffInfo {
    /**
     * id работника.
     */
    private Long id;


    /**
     * Имя работника.
     */
    private String firstName;

    /**
     * Отчество работника.
     */
    private String secondName;

    /**
     * Фамилия работника.
     */
    private String lastName;

    /**
     * Должность работника.
     */
    private String position;

    /**
     * Password.
     */
    private String password;

}
