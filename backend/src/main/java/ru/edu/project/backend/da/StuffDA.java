package ru.edu.project.backend.da;

import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import ru.edu.project.backend.model.Grades;
import ru.edu.project.backend.model.StudentInfo;
import ru.edu.project.backend.model.StuffInfo;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Data Access слой.
 */
@Component
@Profile("JDBC_TEMPLATE")
public class StuffDA implements StuffDALayer {
    /**
     * javadoc.
     */
    private static Logger logger = LoggerFactory.getLogger(StuffDA.class);

    /**
     * Запрос поиска записей по списку id.
     */
    public static final String SELECT_STUFF_BY_ID = "SELECT * FROM STUFF WHERE stuff_id=?";

    /**
     * Запрос поиска успеваемости по id.
     */
    public static final String SELECT_GRADES_BY_ID = "SELECT * FROM GRADES WHERE student_id=?";

    /**
     * Запрос пароля по id.
     */
    public static final String SELECT_PASSWORD_BY_ID = "SELECT password, position FROM STUFF WHERE stuff_id=?";

    /**
     * Запрос списка студентов.
     */
    public static final String SELECT_ALL_STUDENTS = "SELECT * FROM STUDENTS";

    /**
     * Запрос среднего балла по id студента.
     */
    public static final String SELECT_STUDENT_AVERAGE = "SELECT AVG(grade) FROM GRADES WHERE student_id=?";

    /**
     * Запрос списка студентов.
     */
    public static final String SELECT_STUDENTS_BY_GROUP_ID = "SELECT * FROM STUDENTS WHERE group_id=?";

    /**
     * Зависимость на шаблон jdbc.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Зависимость на шаблон jdbc named.
     */
    @Autowired
    private NamedParameterJdbcTemplate jdbcNamed;

    /**
     * Получаем услугу по id.
     *
     * @param id
     * @return job
     */
    @Override
    public StuffInfo getStuffInfoById(final Long id) {
        logger.info(id.toString());
        return jdbcTemplate.query(SELECT_STUFF_BY_ID, this::singleRowMapper, id);
    }

    @SneakyThrows
    private StuffInfo singleRowMapper(final ResultSet resultSet) {
        resultSet.next();
        return mapRow(resultSet);
    }

    @SneakyThrows
    private StuffInfo mapRow(final ResultSet resultSet) {
        return StuffInfo.builder()
                .id(resultSet.getLong("stuff_id"))
                .firstName(resultSet.getString("first_name"))
                .secondName(resultSet.getString("second_name"))
                .lastName(resultSet.getString("last_name"))
                .position(resultSet.getString("position"))
                .build();
    }

    /**
     * Получение student progress по id.
     *
     * @param id
     * @return
     */
    @Override
    public List<Grades> getProgressById(final Long id) {
        return jdbcTemplate.query(SELECT_GRADES_BY_ID, this::gradesRowMapper, id);
    }

    @SneakyThrows
    private Grades gradesRowMapper(final ResultSet resultSet, final int pos) {
        return Grades.builder()
                .student_id(resultSet.getLong("student_id"))
                .group_id(resultSet.getLong("group_id"))
                .subject_title(resultSet.getString("subject_title"))
                .grade(resultSet.getDouble("grade"))
                .build();
    }

    /**
     * получить пароль  по id.
     *
     * @param id
     * @return
     */
    @Override
    public Map<String, String> getPasswordById(Long id) {
        return jdbcTemplate.query(SELECT_PASSWORD_BY_ID, this::StringMapper, id).get(0);
    }

    @SneakyThrows
    private Map<String, String> StringMapper(ResultSet resultSet, int i) {
        Map<String, String> response = new HashMap<>();
        response.put("role", resultSet.getString("position"));
        response.put("password", resultSet.getString("password"));
        return response;
    }


    /**
     * получить пароль  по id.
     *
     * @return
     */
    @Override
    public List<StudentInfo> getAllStudents() {
        List<StudentInfo> list = jdbcTemplate.query(SELECT_ALL_STUDENTS, this::studentMapRow);
        for(StudentInfo student: list) {
            Long id = student.getId();
            Double average = jdbcTemplate.query(SELECT_STUDENT_AVERAGE, this::doubleMapper, id).get(0);
            student.setAverage(average);
        }
        return list;
    }

    @SneakyThrows
    private Double doubleMapper(ResultSet resultSet, int i) {
        return resultSet.getDouble("AVG(grade)");
    }


    @SneakyThrows
    private List<StudentInfo> studentMapRow(final ResultSet resultSet) {
        List<StudentInfo> list = new ArrayList<>();
        while (resultSet.next()) {
            StudentInfo student = StudentInfo.builder()
                    .id(resultSet.getLong("student_id"))
                    .firstName(resultSet.getString("first_name"))
                    .secondName(resultSet.getString("second_name"))
                    .lastName(resultSet.getString("last_name"))
                    .groupId(resultSet.getLong("group_id"))
                    .build();
            list.add(student);
        }
        return list;
    }

    /**
     * Вывод студентов по номеру группы.
     *
     * @return List<StudentInfo>
     */
    public List<StudentInfo> getStudentsByGroupId(Long groupId) {
        List<StudentInfo> list = jdbcTemplate.query(SELECT_STUDENTS_BY_GROUP_ID, this::studentMapRow, groupId);
        for(StudentInfo student: list) {
            Long id = student.getId();
            Double average = jdbcTemplate.query(SELECT_STUDENT_AVERAGE, this::doubleMapper, id).get(0);
            student.setAverage(average);
        }
        return list;
    }

}
