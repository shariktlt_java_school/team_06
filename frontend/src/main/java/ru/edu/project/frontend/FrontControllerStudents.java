package ru.edu.project.frontend;

import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.project.backend.interfaces.StudentService;
import ru.edu.project.backend.model.Grades;
import ru.edu.project.backend.model.StudentInfo;
import ru.edu.project.util.AuthUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@RequestMapping("/student")
@Controller
public class FrontControllerStudents {
    /**
     * role.
     */
    private static final List<String> ROLE = Collections.singletonList("student");
    /**
     * logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(FrontControllerStudents.class);

    /**
     * Ссылка на сервис студента.
     */
    @Autowired
    private StudentService studentService;


    /**
     * Student authentication.
     *
     * @return view
     */
    @GetMapping()
    public String student() {
        return "student";
    }

    /**
     * Get student info by id.
     *
     * @return modelAndView
     */
    @SneakyThrows
    @GetMapping("/info")
    public ModelAndView info(HttpServletRequest request,
                             HttpServletResponse response) {

        if (!AuthUtil.isAuthenticated(request, ROLE)) {
            response.sendRedirect("/student");
            return null;
        }
        Long id = (Long) request.getSession().getAttribute("id");

        ModelAndView model = new ModelAndView("student_info");
        StudentInfo info = studentService.getDetailedInfo(id);
        LOGGER.info(studentService.getClass().getSimpleName());
        LOGGER.info(info.toString());
        if (info == null) {
            model.setStatus(HttpStatus.NOT_FOUND);
            model.setViewName("task/viewNotFound");
            return model;
        }
        model.addObject("record", info);
        return model;
    }

    /**
     * Get student info by id.
     *
     * @return modelAndView
     */
    @SneakyThrows
    @GetMapping("/progress")
    public ModelAndView progress(HttpServletRequest request,
                                 HttpServletResponse response) {

        if (!AuthUtil.isAuthenticated(request, ROLE)) {
            response.sendRedirect("/student");
            return null;
        }

        Long id = (Long) request.getSession().getAttribute("id");
        ModelAndView model = new ModelAndView("student_progress");
        List<Grades> progress = studentService.getProgress(id);
        if (progress == null) {
            model.setStatus(HttpStatus.NOT_FOUND);
            model.setViewName("task/viewNotFound");
            return model;
        }
        Double averageScore = progress.stream().collect(Collectors.averagingDouble(x -> x.getGrade()));
        model.addObject("averageScore", averageScore);
        model.addObject("student_id", progress.get(0).getStudent_id());
        model.addObject("group_id", progress.get(0).getGroup_id());
        model.addObject("requests", progress);
        model.addObject("recordsCount", progress.size());
        return model;
    }

}
