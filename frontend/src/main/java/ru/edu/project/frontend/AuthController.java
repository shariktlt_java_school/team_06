package ru.edu.project.frontend;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.project.util.AuthenticationManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/auth")
public class AuthController {

    private AuthenticationManager authenticationManager;

    @Autowired
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @GetMapping("/studentAuth")
    public String studentAuth() {
        return "studentAuth";
    }

    @GetMapping("/stuffAuth")
    public String stuffAuth() {
        return "stuffAuth";
    }

    @SneakyThrows
    @PostMapping("/studentAuth/authorize")
    public ModelAndView studentAuthorize(HttpServletRequest request,
                                         HttpServletResponse response,
                                         @RequestParam("id") Long id,
                                         @RequestParam("password") String password){
        HttpSession session = request.getSession();
        String role = authenticationManager.studentAuthorize(id, password);
        if (role != null) {
            session.setAttribute("authObject", role);
            session.setAttribute("id", id);
            response.sendRedirect("/student");
            return null;
        } else {
            session.removeAttribute("authObject");
            ModelAndView model = new ModelAndView("studentAuth");
            model.addObject("message", "Wrong id or password");
            return model;
        }
    }

    @SneakyThrows
    @PostMapping("/stuffAuth/authorize")
    public ModelAndView stuffAuthorize(HttpServletRequest request,
                                       HttpServletResponse response,
                                       @RequestParam("id") Long id,
                                       @RequestParam("password") String password) {
        HttpSession session = request.getSession();
        String role = authenticationManager.stuffAuthorize(id, password);
        if (role != null) {
            session.setAttribute("authObject", role);
            session.setAttribute("id", id);
            response.sendRedirect("/stuff");
            return null;
        } else {
            session.removeAttribute("authObject");
            ModelAndView model = new ModelAndView("stuffAuth");
            model.addObject("message", "Wrong id or password");
            return model;
        }
    }
}
