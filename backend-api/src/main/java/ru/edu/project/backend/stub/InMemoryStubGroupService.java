package ru.edu.project.backend.stub;

import ru.edu.project.backend.model.Group;
import ru.edu.project.backend.model.GroupForm;
import ru.edu.project.backend.interfaces.GroupService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class InMemoryStubGroupService implements GroupService {

    /**
     * Мапа из всех групп.
     **/
    private final ConcurrentHashMap<String, Group> groups =
            new ConcurrentHashMap<>();

    /**
     * Создание группы.
     *
     * @param form Data from Group form
     * @return new Group object
     */
    @Override
    public Group createGroup(final GroupForm form) {
        if (form == null) {
            throw new IllegalArgumentException("Form is null");
        }

        String uuid = UUID.randomUUID().toString();
        Group result = Group.builder()
                .id(uuid)
                .title(form.getTitle())
                .isStarted(form.getIsStarted())
                .build();

        groups.put(uuid, result);
        return result;
    }

    /**
     * Получить все группы из БД.
     *
     * @return list of Group objects
     */
    @Override
    public List<Group> getGroups() {
        return new ArrayList<>(groups.values());
    }

    /**
     * Обновить информацию о группе.
     *
     * @param id   Group id
     * @param form Data from Group form
     * @return updated Group object
     */
    @Override
    public Group updateGroup(final String id, final GroupForm form) {

        Group group = groups.get(id);
        if (group == null) {
            throw new IllegalArgumentException("id not found");
        }
        if (form.getTitle() != null) {
            group.setTitle(form.getTitle());
        }
        if (form.getIsStarted() != null) {
            group.setStarted(form.getIsStarted());
        }

        groups.put(group.getId(), group);
        return group;

    }

    /**
     * Получить информацию о группе.
     *
     * @param id Group id
     * @return Group object
     */
    @Override
    public Group getGroup(final String id) {
        return groups.get(id);
    }

    /**
     * Удалить группу по идентификатору.
     *
     * @param id Group id
     * @return Group was deleted?
     */
    @Override
    public boolean deleteGroup(final String id) {

        if (groups.get(id) != null) {
            groups.remove(id);
            return true;
        } else {
            return false;
        }

    }
}
