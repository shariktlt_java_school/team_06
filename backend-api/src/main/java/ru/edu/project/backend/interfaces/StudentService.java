package ru.edu.project.backend.interfaces;

import ru.edu.project.backend.model.Grades;
import ru.edu.project.backend.model.StudentInfo;

import java.util.List;
import java.util.Map;

public interface StudentService {

    /**
     * Просмотр информации о студенте.
     * @param recordId
     * @return StudentInfo
     */
    StudentInfo getDetailedInfo(final Long recordId);

    /**
     * Просмотр информации об успеваемости студента.
     *
     * @param id
     * @return StudentProgress
     */
    List<Grades> getProgress(final Long id);

    /**
     * получение пароля и роли по id.
     * @param id
     * @return
     */
    Map<String, String> getAuthData(Long id);
}

