package ru.edu.project.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.project.backend.model.Grades;
import ru.edu.project.backend.model.StudentInfo;
import ru.edu.project.backend.interfaces.StudentService;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/student")
public class StudentController implements StudentService {

    /**
     * Делегат для передачи вызова.
     */
    @Autowired
    private StudentService delegate;

    /**
     * Просмотр информации о студенте.
     *
     * @param id
     * @return StudentInfo
     */
    @Override
    @GetMapping("/getDetailedInfo/{id}")
    public StudentInfo getDetailedInfo(@PathVariable("id") final Long id) {
        return delegate.getDetailedInfo(id);
    }

    /**
     * Просмотр информации об успеваемости студента.
     *
     * @param id
     * @return StudentProgress
     */
    @Override
    @GetMapping("/getProgress/{id}")
    public List<Grades> getProgress(@PathVariable("id") final Long id) {
        return delegate.getProgress(id);
    }

    /**
     * получение пароля по id.
     *
     * @param id
     * @return
     */
    @Override
    @GetMapping("/getAuthData/{id}")
    public Map<String, String> getAuthData(@PathVariable("id") final Long id) {
        return delegate.getAuthData(id);
    }

}
