package ru.edu.project.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.model.Grades;
import ru.edu.project.backend.model.StudentInfo;
import ru.edu.project.backend.interfaces.StudentService;
import ru.edu.project.backend.da.StudentDALayer;

import java.util.List;
import java.util.Map;

@Service
@Profile("!STUB")
@Qualifier("StudentServiceLayer")
public class StudentServiceLayer implements StudentService {

    /**
     * Зависимость для слоя доступа к данным услуг.
     */
    @Autowired
    private StudentDALayer daLayer;

    /**
     * Просмотр информации о студенте.
     *
     * @param id
     * @return StudentInfo
     */
    @Override
    public StudentInfo getDetailedInfo(final Long id) {
        return daLayer.getStudentInfoById(id);
    }

    /**
     * Просмотр информации об успеваемости студента.
     *
     * @param id
     * @return StudentProgress
     */
    @Override
    public List<Grades> getProgress(final Long id) {
        return daLayer.getProgressById(id);
    }

//    /**
//     * Вывод всех студентов.
//     *
//     * @return List
//     */
//    @Override
//    public List<StudentInfo> getAllStudents() {
//        return null;
//    }

    /**
     * получение пароля по id.
     *
     * @param id
     * @return
     */
    @Override
    public Map<String, String> getAuthData(final Long id) {
        return daLayer.getPasswordById(id);
    }
}
