package ru.edu.project.backend.da;

import ru.edu.project.backend.model.Grades;
import ru.edu.project.backend.model.StudentInfo;

import java.util.List;
import java.util.Map;

public interface StudentDALayer {

    /**
     * Получение student info по id.
     *
     * @param id
     * @return job
     */
    StudentInfo getStudentInfoById(Long id);

    /**
     * Получение student progress по id.
     * @param id
     * @return э
     */
    List<Grades> getProgressById(Long id);

    /**
     * получить пароль  по id.
     * @param id
     * @return
     */
    Map<String, String> getPasswordById(Long id);
}
