package ru.edu.project.backend.da;

import ru.edu.project.backend.model.StudentInfo;
import ru.edu.project.backend.model.StuffInfo;

public interface AdminDALayer{

    /**
     * Добавить студента.
     * @return StudentInfo
     */
    int addNewStudent(final StudentInfo student);

    /**
     * Удалить студента.
     * @return StudentInfo
     */
    int deleteStudent(final Long id);

    /**
     * Добавить сотрудника.
     * @return StuffInfo
     */
    int addNewStuff(final StuffInfo stuff);

    /**
     * Удалить сотрудника.
     * @return StuffInfo
     */
    int deleteStuff(final Long id);
}
