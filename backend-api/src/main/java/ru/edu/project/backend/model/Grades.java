package ru.edu.project.backend.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Setter
@Builder
@Jacksonized
public class Grades {
    /**
     * smth.
     */
    private Long student_id;
    /**
     * smth.
     */
    private Long group_id;
    /**
     * smth.
     */
    private String subject_title;
    /**
     * smth.
     */
    private Double grade;
}
