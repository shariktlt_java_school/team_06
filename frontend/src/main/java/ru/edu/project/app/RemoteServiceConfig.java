package ru.edu.project.app;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import ru.edu.project.backend.RestServiceInvocationHandler;
import ru.edu.project.backend.interfaces.AdminService;
import ru.edu.project.backend.interfaces.StudentService;
import ru.edu.project.backend.interfaces.StuffService;

import java.lang.reflect.Proxy;

@Configuration
@Profile("REST")
public class RemoteServiceConfig {

    /**
     * Создаем rest-прокси для RequestService.
     *
     * @param handler
     * @return rest-proxy
     */
    @Bean
    public StudentService studentRequestServiceRest(final RestServiceInvocationHandler handler) {
        handler.setServiceUrl("/student");
        return getProxy(handler, StudentService.class);
    }

    /**
     * Создаем rest-прокси для RequestService.
     *
     * @param handler
     * @return rest-proxy
     */
    @Bean
    public StuffService stuffRequestServiceRest(final RestServiceInvocationHandler handler) {
        handler.setServiceUrl("/stuff");
        return getProxy(handler, StuffService.class);
    }

    /**
     * Создаем rest-прокси для RequestService.
     *
     * @param handler
     * @return rest-proxy
     */
    @Bean
    public AdminService adminRequestServiceRest(final RestServiceInvocationHandler handler) {
        handler.setServiceUrl("/stuff");
        return getProxy(handler, AdminService.class);
    }

    private <T> T getProxy(final RestServiceInvocationHandler handler, final Class<T>... tClass) {
        return (T) Proxy.newProxyInstance(this.getClass().getClassLoader(), tClass, handler);
    }

}
