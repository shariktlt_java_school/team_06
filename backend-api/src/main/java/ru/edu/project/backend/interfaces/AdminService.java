package ru.edu.project.backend.interfaces;

import ru.edu.project.backend.common.AcceptorArgument;
import ru.edu.project.backend.model.StudentInfo;
import ru.edu.project.backend.model.StuffInfo;

public interface AdminService{

    /**
     * Добавить студента.
     * @return StudentInfo
     */
    @AcceptorArgument
    int createStudent(StudentInfo student);

    /**
     * Удалить студента.
     * @return StudentInfo
     */
    int deleteStudent(Long id);

    /**
     * Добавить сотрудника.
     * @return StuffInfo
     */
    @AcceptorArgument
    int createStuff(StuffInfo stuff);

    /**
     * Удалить сотрудника.
     * @return StuffInfo
     */
    int deleteStuff(Long id);

}
