package ru.edu.project.frontend;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/")
@Controller
public class FrontControllerIndex {

    /**
     * Точка входа.
     *
     * @return view
     */
    @GetMapping("/")
    public String index() {
        return "index";
    }

}
