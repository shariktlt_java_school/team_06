package ru.edu.project.backend.model;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Jacksonized
@Builder
public class GroupForm {

    /**
     * Идентификатор группы.
     */
    private String title;

    /**
     * Флаг начала занятий.
     */
    private Boolean isStarted;

}
