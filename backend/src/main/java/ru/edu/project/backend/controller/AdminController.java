package ru.edu.project.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.edu.project.backend.interfaces.AdminService;
import ru.edu.project.backend.model.StudentInfo;
import ru.edu.project.backend.model.StuffInfo;

@RestController
@RequestMapping("/stuff")
public class AdminController implements AdminService {

    /**
     * Делегат для передачи вызова.
     */
    @Autowired
    private AdminService delegate;

    /**
     * Добавить студента.
     *
     * @param student
     * @return StudentInfo
     */
    @Override
    @PostMapping("/createStudent")
    public int createStudent(@RequestBody final StudentInfo student) {
        return delegate.createStudent(student);
    }

    /**
     * Удалить студента.
     *
     * @param id
     * @return StudentInfo
     */
    @Override
    @GetMapping("/deleteStudent/{id}")
    public int deleteStudent(@PathVariable("id") Long id) {
        return delegate.deleteStudent(id);
    }

    /**
     * Добавить сотрудника.
     *
     * @param stuff
     * @return StuffInfo
     */
    @Override
    @PostMapping("/createStuff")
    public int createStuff(@RequestBody final StuffInfo stuff) {
        return delegate.createStuff(stuff);
    }

    /**
     * Удалить сотрудника.
     *
     * @param id
     * @return StuffInfo
     */
    @Override
    @GetMapping("/deleteStuff/{id}")
    public int deleteStuff(@PathVariable("id") Long id) {
        return delegate.deleteStuff(id);
    }
}
