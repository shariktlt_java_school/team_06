package ru.edu.project.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.edu.project.backend.interfaces.StudentService;
import ru.edu.project.backend.interfaces.StuffService;

import java.util.Map;

;

@Component
public class AuthenticationManager {

    /**
     * Ссылка на сервис студента.
     */
    @Autowired
    private StudentService studentService;

    /**
     * Ссылка на сервис сотрудника.
     */
    @Autowired
    private StuffService stuffService;


    public String studentAuthorize(Long id, String password){
        Map<String, String> authData = studentService.getAuthData(id);
        String dbPass = authData.get("password");
        String dbRole = authData.get("role");
        if(dbPass.equals(password)) {
            return dbRole;
        }
        return null;
    }

    public String stuffAuthorize(Long id, String password){
        Map<String, String> authData = stuffService.getAuthData(id);
        String dbPass = authData.get("password");
        String dbRole = authData.get("role");
        if(dbPass.equals(password)) {
            return dbRole;
        }
        return null;
    }
}
