package ru.edu.project.backend.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.interfaces.GroupService;
import ru.edu.project.backend.da.GroupDALayer;
import ru.edu.project.backend.model.Group;
import ru.edu.project.backend.model.GroupForm;

import java.util.List;

@Service
@Profile("!STUB")
//@Qualifier("RequestServiceLayer")
public class GroupServiceLayer implements GroupService {


    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(GroupServiceLayer.class);


    /**
     * Зависимость для слоя доступа к данным заявок.
     */
    @Autowired
    private GroupDALayer daLayer;

    /**
     * Creating Group.
     *
     * @param form Data from Group form
     * @return new Group object
     */
    @Override
    public Group createGroup(final GroupForm form) {
        Group group = Group.builder()
                .title(form.getTitle())
                .isStarted(form.getIsStarted())
                .build();
        return daLayer.save(group);
    }

    /**
     * Update Group info.
     * @param id Group id
     * @param form Data from Group form
     * @return updated Group object
     */
    @Override
    public Group updateGroup(final String id,
                             final GroupForm form) {

        Group group = Group.builder()
                .title(form.getTitle())
                .isStarted(form.getIsStarted())
                .build();
        group.setId(id);
        return daLayer.save(group);
    }

    /**
     * Get Group's info.
     * @param id Group id
     * @return Group object
     */
    @Override
    public Group getGroup(final String id) {
        return daLayer.getById(id);
    }

    /**
     * Delete Group by id.
     * @param id Group id
     * @return Group was deleted?
     */
    @Override
    public boolean deleteGroup(final String id) {
        return daLayer.deleteById(id);
    }

    /**
     * @return list
     */
    @Override
    public List<Group> getGroups() {
        return daLayer.getGroups();
    }


}
