package ru.edu.project.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.da.StuffDALayer;
import ru.edu.project.backend.interfaces.StuffService;
import ru.edu.project.backend.model.Grades;
import ru.edu.project.backend.model.StudentInfo;
import ru.edu.project.backend.model.StuffInfo;

import java.util.List;
import java.util.Map;

@Service
@Profile("!STUB")
@Qualifier("StuffServiceLayer")
public class StuffServiceLayer implements StuffService {

    /**
     * Зависимость для слоя доступа к данным услуг.
     */
    @Autowired
    private StuffDALayer daLayer;

    /**
     * Просмотр информации о студенте.
     *
     * @param id
     * @return StuffInfo
     */
    @Override
    public StuffInfo getDetailedInfo(final Long id) {
        return daLayer.getStuffInfoById(id);
    }

    /**
     * Просмотр информации об успеваемости студента.
     *
     * @param id
     * @return StudentProgress
     */
    @Override
    public List<Grades> getProgress(final Long id) {
        return daLayer.getProgressById(id);
    }


    /**
     * Вывод всех студентов.
     *
     * @return List
     */
    @Override
    public List<StudentInfo> getAllStudents() {
        return daLayer.getAllStudents();
    }

    /**
     * получение пароля по id.
     *
     * @param id
     * @return
     */
    @Override
    public Map<String, String> getAuthData(final Long id) {
        return daLayer.getPasswordById(id);
    }

    /**
     * Вывод студентов по номеру группы.
     *
     * @return List<StudentInfo>
     */
    public List<StudentInfo> getStudentsByGroupId(Long groupId) {
        return daLayer.getStudentsByGroupId(groupId);
    }
}
