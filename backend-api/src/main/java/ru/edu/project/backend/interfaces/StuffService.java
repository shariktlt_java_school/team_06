package ru.edu.project.backend.interfaces;

import ru.edu.project.backend.model.Grades;
import ru.edu.project.backend.model.StudentInfo;
import ru.edu.project.backend.model.StuffInfo;

import java.util.List;
import java.util.Map;

public interface StuffService {

    /**
     * Просмотр информации о работнике.
     * @param id
     * @return StufftInfo
     */
    StuffInfo getDetailedInfo(final Long id);

    /**
     * Просмотр информации об успеваемости студента.
     *
     * @param id
     * @return StudentProgress
     */
    List<Grades> getProgress(final Long id);

    /**
     * Вывод всех студентов.
     * @return List<StudentInfo>
     */
    List<StudentInfo> getAllStudents();

    /**
     * получение пароля по id.
     * @param id
     * @return
     */
    Map<String, String> getAuthData(Long id);

    /**
     * Вывод студентов по номеру группы.
     * @return List<StudentInfo>
     */
    List<StudentInfo> getStudentsByGroupId(Long groupId);


}

