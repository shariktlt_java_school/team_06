package ru.edu.project.frontend;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.project.backend.interfaces.AdminService;
import ru.edu.project.backend.model.StudentInfo;
import ru.edu.project.backend.model.StuffInfo;
import ru.edu.project.util.AuthUtil;
import ru.edu.project.util.PasswordGenerator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

@Controller
public class FrontControllerAdmin {
    private static final String ADMIN = "admin";
    /**
     * Ссылка на сервис сотрудника.
     */
    @Autowired
    private AdminService adminService;

    /**
     * Stuff authentication.
     *
     * @return view
     */
    @SneakyThrows
    @GetMapping("/admin")
    public String admin(HttpServletRequest request,
                        HttpServletResponse response) {
        if (!AuthUtil.isAuthenticated(request, Arrays.asList(ADMIN))) {
            response.sendRedirect("/stuff");
            return null;
        }
        return "admin";
    }

    /**
     * Add student.
     */
    @SneakyThrows
    @GetMapping("/addStudent")
    public ModelAndView addStudent(HttpServletRequest request,
                                       HttpServletResponse response,
                                       @RequestParam("first_name") String firstName,
                                       @RequestParam("second_name") String second_name,
                                       @RequestParam("last_name") String last_name) {

        if (!AuthUtil.isAuthenticated(request, Arrays.asList(ADMIN))) {
            response.sendRedirect("/stuff");
            return null;
        }
        String password = PasswordGenerator.generateText();
        StudentInfo student = StudentInfo.builder()
                .firstName(firstName)
                .secondName(second_name)
                .lastName(last_name)
                .password(password)
                .build();
        int id = adminService.createStudent(student);
        ModelAndView model = new ModelAndView("admin");
        model.addObject("message1", String.format("Студент успешно создан. ID: %d. Пароль: %s", id, password));
        return model;
    }

    /**
     * Delete student.
     */
    @SneakyThrows
    @GetMapping("/deleteStudent")
    public ModelAndView deleteStudent(HttpServletRequest request,
                             HttpServletResponse response,
                             @RequestParam("student_id") Long id) {

        if (!AuthUtil.isAuthenticated(request, Arrays.asList(ADMIN))) {
            response.sendRedirect("/stuff");
            return null;
        }
        adminService.deleteStudent(id);
        ModelAndView model = new ModelAndView("admin");
        model.addObject("message1", "Студент с ID: " + id + " успешно удален");
        return model;
    }

    /**
     * Add stuff.
     */
    @SneakyThrows
    @GetMapping("/addStuff")
    public ModelAndView addStuff(HttpServletRequest request,
                                       HttpServletResponse response,
                                       @RequestParam("first_name") String firstName,
                                       @RequestParam("second_name") String second_name,
                                       @RequestParam("last_name") String last_name,
                                        @RequestParam("position") String position) {

        if (!AuthUtil.isAuthenticated(request, Arrays.asList(ADMIN))) {
            response.sendRedirect("/stuff");
            return null;
        }
        String password = PasswordGenerator.generateText();
        StuffInfo stuff = StuffInfo.builder()
                .firstName(firstName)
                .secondName(second_name)
                .lastName(last_name)
                .position(position)
                .password(password)
                .build();
        int id = adminService.createStuff(stuff);
        ModelAndView model = new ModelAndView("admin");
        model.addObject("message2", String.format("Сотрудник успешно создан. ID: %d. Пароль: %s", id, password));
        return model;
    }

    /**
     * Delete stuff.
     */
    @SneakyThrows
    @GetMapping("/deleteStuff")
    public ModelAndView deleteStuff(HttpServletRequest request,
                                      HttpServletResponse response,
                                      @RequestParam("stuff_id") Long id) {

        if (!AuthUtil.isAuthenticated(request, Arrays.asList(ADMIN))) {
            response.sendRedirect("/stuff");
            return null;
        }
        adminService.deleteStuff(id);
        ModelAndView model = new ModelAndView("admin");
        model.addObject("message2", "Сотрудник с ID: " + id + " успешно удален");
        return model;
    }

}
