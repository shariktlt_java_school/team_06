package ru.edu.project.util;

public class PasswordGenerator {

    public static String generateText() {
        int length = (int) (Math.random() * 10) + 10;
        char[] charArray = new char[length];

        for (int i = 0; i < length; i++) {
            charArray[i] = (char) ((int) (Math.random() * 25) + 97);
        }
        return new String(charArray);
    }
}
