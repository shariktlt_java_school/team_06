package ru.edu.project.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import ru.edu.project.backend.model.Group;
import ru.edu.project.backend.model.GroupForm;
import ru.edu.project.backend.interfaces.GroupService;

import java.util.List;

@RestController
@RequestMapping("/group")
public class GroupController implements GroupService {

    /**
     * Делегат для передачи вызова.
     */
    @Autowired
    private GroupService delegate;

    /**
     * Создание группы.
     *
     * @param form Data from Group form
     * @return new Group object
     */
    @Override
    @PostMapping("/createGroup")
    public Group createGroup(@RequestBody final GroupForm form) {
        return delegate.createGroup(form);
    }

    /**
     * Получить все группы из БД.
     * @return list of Group objects
     */
    @Override
    @GetMapping("/getGroups")
    public List<Group> getGroups() {
        return delegate.getGroups();
    }

    /**
     * Обновить информацию о группе.
     * @param id Group id
     * @param form Data from Group form
     * @return updated Group object
     */
    @Override
    @PostMapping("/updateGroup/{id}")
    public Group updateGroup(@PathVariable("id") final String id,
                             @RequestBody final GroupForm form) {
        return delegate.updateGroup(id, form);
    }

    /**
     * Получить информацию о группе.
     * @param id Group id
     * @return Group object
     */
    @Override
    @GetMapping("/getGroup/{id}")
    public Group getGroup(@PathVariable("id") final String id) {
        return delegate.getGroup(id);
    }

    /**
     * Удалить группу по идентификатору.
     * @param id Group id
     * @return Group was deleted?
     */
    @Override
    @PostMapping("/deleteGroup/{id}")
    public boolean deleteGroup(@PathVariable("id") final String id) {
        return delegate.deleteGroup(id);
    }

}
